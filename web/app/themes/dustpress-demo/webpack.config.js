const { ESBuildMinifyPlugin } = require('esbuild-loader')

module.exports = {
    mode:'production',
    entry:{
        'theme':'./src/ts/theme.tsx'
    },
    watch:true,
    output:{
        filename: '../assets/scripts/[name].js',
    },
    optimization: {
        minimize: true,
        minimizer: [new ESBuildMinifyPlugin({target: 'es2015'})],
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.jsx', '.js', '.json']
    },
    module: {
        rules: [
            {
                test: [/\.tsx?$/,/\.ts?$/],
                exclude: /node_modules/,
                loader: 'esbuild-loader',
                options: {
                    loader: 'tsx',
                    target: 'es2015'
                }
            },
            {
                test: /\.css$/i,
                use: [
                    "style-loader",
                    "css-loader",
                    {
                        loader: "postcss-loader",
                        options: {
                        postcssOptions: {
                            plugins: [
                            [
                                "postcss-preset-env",
                                {
                                // Options
                                },
                            ],
                            ],
                        },
                        },
                    },
                ],
              },
        ]
    }
}