<?php 

\add_action('admin_init', function(){
    register_setting('general', 'options_page_id', 'esc_attr');
    add_settings_field(
        'options_page_id',
        'Options Page Id',
        function($args){
            echo "<input type='text' id='options_page_id' name='options_page_id' value='".get_option('options_page_id','29')."' />";
        },
        'general',
        'default',
        array( 'label_for' => 'options_page_id' )
    );
});

\add_filter('use_block_editor_for_post', function($can, $post){
    if($post){
        $settingsid = (int) get_option('options_page_id','29');
        if($post->ID === $settingsid){
            return false;
        }
    }
    return $can;
}, 5, 2);
\add_action('pre_get_posts', function($query) {
    if ( !is_admin() ) return;
    $settingsid = (int) get_option('options_page_id','29');
    $query->set( 'post__not_in', array($settingsid) );    
    return;
});
\add_action( 'admin_menu', function(){
    $settingsid = (int) get_option('options_page_id','29');
    add_menu_page( 'Site Settings', 'Site Settings', 'manage_options', 'post.php?post='.strval($settingsid).'&action=edit', '', 'dashicons-admin-tools', 2);
});
\add_filter('parent_file', function($file) {
    global $pagenow;
    $settingsid = (int) get_option('options_page_id','29');
    $post = isset($_GET["post"]) ? (int)$_GET["post"] : null;
    if ($pagenow === "post.php" && $post === $settingsid) {
        $file = "post.php?post=$settingsid&action=edit";
    }
    return $file;
});
\add_action( 'admin_title', function() {
    global $post, $title, $action, $current_screen;
    $settingsid = (int) get_option('options_page_id','29');
    if( isset( $current_screen->post_type ) && $current_screen->post_type === 'page' && $action == 'edit' && $post->ID === $settingsid) {
        $title = $post->post_title;           
    }
    return $title;  
});
\add_action('admin_enqueue_scripts', function(){
    global $post;
    $settingsid = (int) get_option('options_page_id','29');
    if( ! is_a($post, 'WP_Post') ) return;
    if($post->ID==$settingsid){
        remove_post_type_support( 'page', 'editor' );
    }
});

\add_action('acf/init',function(){
    $settingsid = (int) get_option('options_page_id','29');
    if( function_exists('acf_add_local_field_group') ){ 
        acf_add_local_field_group(array(
            'key' => 'group_6313cfd4b4cb4',
            'title' => 'Theme Settings',
            'fields' => array(
                
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'page',
                        'operator' => '==',
                        'value' => strval($settingsid),
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            'show_in_rest' => 0,
        ));
        acf_add_local_field_group(array(
            'key' => 'group_63144e62d82dd',
            'title' => 'Event Fields',
            'fields' => array(
                array(
                    'key' => 'field_63144e6987298',
                    'label' => 'Start date',
                    'name' => 'start_date',
                    'type' => 'date_time_picker',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'display_format' => 'd/m/Y g:i a',
                    'return_format' => 'd.m.Y H:i',
                    'first_day' => 1,
                ),
                array(
                    'key' => 'field_63144e8487299',
                    'label' => 'End Date',
                    'name' => 'end_date',
                    'type' => 'date_time_picker',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'display_format' => 'd/m/Y g:i a',
                    'return_format' => 'd.m.Y H:i',
                    'first_day' => 1,
                ),
                array(
                    'key' => 'field_6314cf3823535',
                    'label' => 'Featured Image',
                    'name' => '_thumbnail_id',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'url',
                    'preview_size' => 'medium',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array(
                    'key' => 'field_6314cf3823535',
                    'label' => 'Featured Image',
                    'name' => '_thumbnail_id',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'url',
                    'preview_size' => 'medium',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array(
                    'key' => 'field_6314d37b67282',
                    'label' => 'Excerpt',
                    'name' => 'excerpt',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => 180,
                    'rows' => '',
                    'new_lines' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'event',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            'show_in_rest' => 0,
        ));
    }	
},100);

?>