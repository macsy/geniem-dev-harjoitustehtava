<?php 
\add_action( 'init', function(){
    \register_post_type( "event", [
        "label" => __( "Events", "dustpress-demo" ),
        "labels" => [
            "name" => __( "Events", "dustpress-demo" ),
            "singular_name" => __( "Event", "dustpress-demo" ),
        ],
        "description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "event", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "author", "page-attributes", "post-formats" ],
		"taxonomies" => [ "category" ],
		"show_in_graphql" => false,
    ] );
});
?>