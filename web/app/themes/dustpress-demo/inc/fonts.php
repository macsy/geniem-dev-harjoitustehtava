<?php 

function dustpressdemo_load_font_faces(){ ?>
<style>
    @font-face {
        font-family: Roboto;
        font-style: normal;
        font-weight: 900;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-Black.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: italic;
        font-weight: 900;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-BlackItalic.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: normal;
        font-weight: 700;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-Bold.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: italic;
        font-weight: 700;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-BoldItalic.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: normal;
        font-weight: 500;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-Medium.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: italic;
        font-weight: 500;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-MediumItalic.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: normal;
        font-weight: 400;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-Regular.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: italic;
        font-weight: 400;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-Italic.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: normal;
        font-weight: 300;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-Thin.ttf');
    }
    @font-face {
        font-family: Roboto;
        font-style: italic;
        font-weight: 300;
        font-display: swap;
        src: url('<?php echo ASSETS_DIR; ?>/fonts/Roboto-ThinItalic.ttf');
    }
</style>
<?php }
\add_action('wp_head','dustpressdemo_load_font_faces',100);
\add_action('admin_head','dustpressdemo_load_font_faces',100);
?>