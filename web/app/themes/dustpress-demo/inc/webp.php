<?php 

add_filter('mime_types', function($existing_mimes) {
    $existing_mimes['webp'] = 'image/webp';
    return $existing_mimes;
});
add_filter('file_is_displayable_image', function($result, $path) {
    if ($result === false) {
        $displayable_image_types = array( IMAGETYPE_WEBP );
        $info = @getimagesize( $path );
        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }
    return $result;
}, 10, 2);
?>