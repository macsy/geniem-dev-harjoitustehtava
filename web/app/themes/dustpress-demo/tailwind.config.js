/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./partials/**/*.dust",
    "./assets/**/*.{html,js,css}",
    "./inc/**/*.{html,js,css,php}",
    "./src/**/*.{html,js,css,php,ts,tsx}"
  ],
  theme: {
    extend: {
      fontFamily: {
        'body': ['Roboto', 'system-ui'],
      }
    },
  },
  plugins: [],
}
