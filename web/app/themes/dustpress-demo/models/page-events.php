<?php
/**
 * Template Name: Events
 *
 * This template needs a page to function!
 */

/**
 * Class Archive
 */
class PageEvents extends MiddleModel {

    /**
     * Enable DustPress.js usage
     *
     * @var array
     */
    protected $api = [
        'AllEvents',
        'UpcomingEvents'
    ];
    
    /**
     * Query posts for the archive page.
     * This function also handles pagination.
     *
     * @return array|bool|WP_Query
     */
    public function AllEvents() {
        $args = (array) $this->get_args();
        $page = isset( $args['page'] ) ? $args['page'] : 1;
        return $this->get_all_events( $page, 2  );
    }

    /**
     * Query posts for the archive page.
     * This function also handles pagination.
     *
     * @return array|bool|WP_Query
     */
    public function UpcomingEvents() {
        $args = (array) $this->get_args();
        $page = isset( $args['page'] ) ? $args['page'] : 1;
        return $this->get_upcoming_events( $page, 2 );
    }


}
