<?php
/**
 * Template Name: Archive
 *
 * This template needs a page to function!
 */

/**
 * Class Archive
 */
class SingleEvent extends MiddleModel {

    /**
     * Enable DustPress.js usage
     *
     * @var array
     */
    protected $api = [
        'Event'
    ];
    
    /**
     * Query posts for the archive page.
     * This function also handles pagination.
     *
     * @return array|bool|WP_Query
     */
    public function Event() {
        return $this->get_single_event( get_the_ID() );
    }
}
