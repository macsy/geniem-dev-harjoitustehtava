import * as React from 'react'
import { Box, Tab, Tabs } from "@mui/material"
import Typography from '@mui/material/Typography'
import { includes, map } from 'lodash-es'
import { useAlert } from 'react-alert'

interface TabPanelProps {
  children?: React.ReactNode
  index: number
  value: number
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  }
}

interface TabsI {
    tabdata:{
        tabtitle:string
        tabcontent:string
    }[]
}

const TabWithData = ({tabcontent,value,index}) => {
    const [content,setContent] = React.useState(tabcontent)
    const [showLoadMore,setShowLoadMore] = React.useState(true)
    const [currentPage,setCurrentPage] = React.useState(1)
    const alert = useAlert()

    const loadMoreUpcomingEvents = () => {
        window.dp("PageEvents/UpcomingEvents", {
            args: {
                page: currentPage+1
            },
            tidy: true,
            partial: "post-list"
        }).then((response:any)=>{
            setCurrentPage(currentPage+1)
            if(response?.success){
                setShowLoadMore(includes(response?.success,'post-list-item'))
                setContent(content+response?.success)
                if(!includes(response?.success,'post-list-item')){
                    alert.info('Kaikki sisältö on nyt ladattu.')
                }
            }
        }).catch(function( error:Error ) {
            console.error(error)
        })
        return false
    }
    const loadMoreAllEvents = () => {
        window.dp("PageEvents/AllEvents", {
            args: {
                page: currentPage+1
            },
            tidy: true,
            partial: "post-list"
        }).then((response:any)=>{
            setCurrentPage(currentPage+1)
            if(response?.success){
                setShowLoadMore(includes(response?.success,'post-list-item'))
                setContent(content+response?.success)
                if(!includes(response?.success,'post-list-item')){
                    alert.info('Kaikki sisältö on nyt ladattu.')
                }
            }
        }).catch(function( error:Error ) {
            console.error(error)
        })
        return false
    }

    return <TabPanel value={value} index={index}>
        <div className="w-full block" dangerouslySetInnerHTML={{__html:content}} />
        {showLoadMore && <div className="w-full flex flex-row flex-wrap justify-center items-center my-2">
            <button className='inline-block px-3 py-2 rounded bg-teal-800 text-neutral-50 mt-4 mb-7' 
            onClick={e=>{
                e.preventDefault()
                e.stopPropagation()
                if(index==0) loadMoreUpcomingEvents()
                else loadMoreAllEvents()
                
            }}>Lataa lisää</button>
        </div>}
    </TabPanel>
}

export const EventTabs:React.FC<TabsI> = ({tabdata}) => {
    const [value, setValue] = React.useState(0)
    const handleChange = (event: React.SyntheticEvent, newValue: number) => {
      setValue(newValue)
    }
    return (
      <div className="w-full block">
        <div className="w-full block max-w-4xl mx-auto px-3">
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                    {map(tabdata,(t,i)=>(<Tab key={`tab-${i}`} label={t?.tabtitle} 
                    {...a11yProps(i)} />))}
                </Tabs>
            </Box>
            {map(tabdata,(t,i)=>(<TabWithData tabcontent={t?.tabcontent} key={`tabpanel-${i}`} value={value} index={i} />))}
        </div>
      </div>
    )
}