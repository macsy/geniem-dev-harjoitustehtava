import styled from "@emotion/styled"
import { isNil, map } from "lodash-es"
import { FC } from "react"
import {createRoot} from 'react-dom/client'
import '../css/input.css'
import * as React from 'react'
import { EventTabs } from "./TabPanel"
import Nav from "./Nav"
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import { createTheme, ThemeProvider } from "@mui/material"
import { blue, teal } from "@mui/material/colors"

interface AppStatic {
    currentPage:number
    currentUpcomingEventsPage:number
    currentAllEventsPage:number
    cache:null|(()=>void)
    init:null|(()=>void)
    $mainContainer:JQuery<HTMLElement>
    $postsContainer:JQuery<HTMLElement>
    $loadMore:any
    $loadMoreUpcomingEvents:any
    $loadMoreAllEvents:any
    maxNumPages:number
    maxNumUpcomingEventsPages:number
    maxNumAllEventsPages:number
}

declare global {
    interface Window {
        dp:any
        DustPressStarter:AppStatic
    }
}

const options = {
    position: positions.BOTTOM_CENTER,
    timeout: 5000,
    offset: '30px',
    transition: transitions.SCALE
}

const theme = createTheme({
    palette: {
        primary:teal,
        secondary:blue,
    },
})

window.DustPressStarter = (function(window, document, $) {

    var app:AppStatic = {
        currentPage: 1,
        currentUpcomingEventsPage: 1,
        currentAllEventsPage: 1,
        cache:null,
        init:null,
        $mainContainer:$('html'),
        $postsContainer:$('html'),
        $loadMore:$('html'),
        $loadMoreUpcomingEvents:$('html'),
        $loadMoreAllEvents:$('html'),
        maxNumPages:0,
        maxNumUpcomingEventsPages:0,
        maxNumAllEventsPages:0,
    };

    app.cache = function () {
        app.$mainContainer  = $("#main-content");
        app.$postsContainer = $("#post-list-container");
        app.$loadMore       = app.$mainContainer.find("#load-more");
        app.$loadMoreUpcomingEvents       = app.$mainContainer.find("#load-more-upcoming-events");
        app.$loadMoreAllEvents       = app.$mainContainer.find("#load-more-all-events");
        app.maxNumPages     = parseInt(app.$loadMore.data('max-num-pages'));
        app.maxNumUpcomingEventsPages     = parseInt(app.$loadMoreUpcomingEvents.data('max-num-pages'));
        app.maxNumAllEventsPages     = parseInt(app.$loadMoreAllEvents.data('max-num-pages'));
    };

    app.init = function() {
        $('body').children('li').remove()
        if(!isNil(app.cache)) app.cache();

        $('.react-nav').each((i,el)=>{
            const items = $(el).children('ul').children('li').map((i,li)=>{
                const object_id = $(li).data('object_id') ?? ''
                const url = $(li).data('url') ?? ''
                const title = $(li).data('title') ?? ''
                return {object_id,url,title}
            }).get()
            const root = createRoot(el)
            root.render(<Nav navdata={items} />)
        })
        $('.react-tabs').each((i,el)=>{
            const items = $(el).children('.react-tab').map((i,tab)=>{
                const tabtitle = $(tab).children('h2').html()
                const tabcontent = $(tab).children('div').html()
                return {tabtitle,tabcontent}
            }).get()
            const root = createRoot(el)
            root.render(<AlertProvider template={AlertTemplate} {...options}>
                <ThemeProvider theme={theme}>
                    <EventTabs tabdata={items} />
                </ThemeProvider>
            </AlertProvider>)
        })
    };
    app.init()
    return app
}(window, document, jQuery))

export {}