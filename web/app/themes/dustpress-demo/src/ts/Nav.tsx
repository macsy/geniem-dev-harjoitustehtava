import styled from "@emotion/styled"
import { map } from "lodash-es"
import * as React from 'react'

interface NavI {
    navdata:{
        object_id:number
        url:string
        title:string
    }[]
}


const NavBar = styled.div`
    width:100%;
    display:flex;
    flex-flow:row wrap;
    justify-content:center;
    align-items:center;
    position:fixed;
    left:0;
    top:0;
    right:0;
    height:50px;
    body.admin-bar & {
        top:32px;
        @media screen and (max-width: 782px) {
            top:46px;
        }
    }
`

const Nav:React.FC<NavI> = ({navdata}) => {
    console.log("navdata",navdata)
    return <NavBar className="bg-neutral-50/[.85] shadow backdrop-blur-md z-10">
        <div className="w-full max-w-4xl mx-auto flex flex-row flex-wrap justify-center items-center text-teal-800">
            <a href="/" className="inline-flex flex-row flex-wrap items-center justify-start flex-shrink mr-4">
                <span className="material-symbols-outlined mr-2">
                edit
                </span>
                Geniem harjoitustehtävä
            </a>
            <nav className="block flex-grow py-1" {...{'data-navdata':JSON.stringify(map(navdata,n=>JSON.stringify(n)))}}>
                <ul className="w-full flex flex-row flex-wrap justify-start items-center">
                    {map(navdata,(n,i)=>(<li className="inline-block px-1 mx-1" key={`navitem-${i}`}>
                        <a className="inline-block px-3 py-2 rounded shadow bg-transparent border border-teal-800" 
                        href={n?.url}>{n?.title}</a>
                    </li>))}
                </ul>
            </nav>
            <div className="block flex-grow" />
            <a className="hidden sm:inline-flex flex-row flex-wrap items-center justify-start flex-shrink mr-4" 
            href="https://www.linkedin.com/in/joona-m%C3%A4kinen-313647106/">
                <span className="icon mr-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width={24} height={24} fill="currentColor" className="bi bi-linkedin" 
                    viewBox="0 0 16 16">
                        <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z"/>
                    </svg>
                </span>
                Joona Mäkinen
            </a>
        </div>
    </NavBar>
}
export default Nav