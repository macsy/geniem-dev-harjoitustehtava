# DustPress Demo - Joona Mäkinen

Built from the starter theme, it loads dustpress and registers some required templates. With Webpack and React ofc.

## Install

The theme itself would be installed just like any other. Download the contents of this repository to your wp-content/themes directory.

Add DustPress in the mix by requiring it via `composer` and using Composer's autoloading feature or simply clone its [repository](https://github.com/devgeniem/dustpress) to your preferred destination and require the `dustpress.php` file in the beginning of the `functions.php` of the theme.

Then just activate the theme from the WordPress admin panel, and you are good to go!