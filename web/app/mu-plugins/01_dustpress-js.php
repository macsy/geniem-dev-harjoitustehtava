<?php
/**
 * Plugin Name: DustPress.JS
 * Plugin URI: https://github.com/devgeniem/dustpress-js
 * Description: DustPress JavaScript library. Provides a front-end interface for interacting with the backend functions.
 * Version: 4.4.2
 * Requires at least: 6.0
 * Tested up to: 6.0.2
 * Requires PHP: 8.0
 * Author: Geniem Oy / Miika Arponen, Ville Siltala & Arttu Mäkipörhölä
 * Author URI: http://www.geniem.com
 */
require_once __DIR__.'/dustpress-js/plugin.php';
?>