# DustPress - Joona Mäkinen

Harjoitustehtävä toimitettuna WP juurena.

## Core

WP Core ja pluginit käyttää Roots Bedrock-mallia, jossa pluginit ladataan WPackagist-palvelusta. Dustpress-js on mu-pluginina.

## Teema

Dustpress-templaattien ja modelien lisäksi Webpack kokoaa käytetyt Tailwind-luokat (postcss) sekä sivuston React-toiminnallisuuden yhdeksi JS-tiedostoksi. 

Valikon osaset laittaa `data-` attribuutteihin tarvittavat tiedot, ja niiden avulla valikko parsetaan React-komponentiksi. Rekursiivinen monitasoisuus ei ole mukana tässä projektissa koska sitä ei tarvita, valikolla ei ole submenuja.

Välilehdet ja sisällön AJAX hakeminen on toteutettu myös Reactilla.